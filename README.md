# PyTorch Template

This repository is a template for Machine Learning models implemented in PyTorch.
It uses [Comet](https://www.comet.ml/) to log parameters and metrics.


## Setting Environment with Conda

Run

```
$ conda env create --file environment.yml
```

## Data

<!-- TODO Write the steps for connect to the data -->

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.

## Related Projects

- https://github.com/moemen95/PyTorch-Project-Template

