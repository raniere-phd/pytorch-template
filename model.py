"""
Model

Include the definition of the model.
"""
import torch
import torch.nn as nn


class Model(nn.Module):
    """Model"""

    def __init__(self, config):
        """Create instance of model."""
        super().__init__()
        self.config = config

        # define layers

        # initialize weights
        self.apply(weights_init)

        raise NotImplementedError

    def forward(self, x):
        raise NotImplementedError
