"""
Agent

Includes all the functions that train, validate, and test the model.
"""
import torch
from torch import nn
import torch.optim as optim

import dataset
import model


class Agent:
    """
    This base class will contain the base functions to be overloaded by any agent you will implement.
    """

    def __init__(self, hyper_params, logger=None):
        self.hyper_params = hyper_params
        self.logger = logger
        self.model = model.Model(self.hyper_params).to(
            self.hyper_params['device'])
        self.data_train_loader = None
        self.data_test_loader = None

    def train(self):
        """
        Main training loop
        :return:
        """
        raise NotImplementedError

    def train_one_epoch(self):
        """
        One epoch of training
        :return:
        """
        raise NotImplementedError

    def validate(self):
        """
        Main validate loop
        :return:
        """
        raise NotImplementedError

    def test(self):
        """
        Main test loop
        :return:
        """
        raise NotImplementedError

    def finalize(self):
        """
        Finalizes all the operations
        :return:
        """
        raise NotImplementedError
